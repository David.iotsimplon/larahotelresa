<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resa', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('nom', 25);          //email de celui qui réserve
            $table->string('prenom', 25);       //phone de celui qui réserve
            $table->string('email', 100);       //email de celui qui réserve
            $table->string('phone', 15);        //phone de celui qui réserve
            $table->integer('personnes');       //nombre d'occupants par chambre
            $table->string('checkin', 15);      // date d'entrée
            $table->string('checkout', 15);     // date de sortie
            $table->integer('room');  // observation

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resa');
    }
}
