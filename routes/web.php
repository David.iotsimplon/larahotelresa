<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');

// pour l'authentifiction
Route::resource('user', 'UserController');

Route::get('users', 'UsersController@getInfos');
Route::post('users', 'UsersController@postInfos');

// formulaire contact
Route::get('contact', 'ContactController@getForm');
Route::post('contact', 'ContactController@postForm');

// pour l'upload de la photo d'une pièce d'identité 
Route::get('photo', 'PhotoController@getForm');
Route::post('photo', 'PhotoController@postForm');

// newsletter
Route::get('email', 'EmailController@getForm');
Route::post('email', ['uses' => 'EmailController@postForm', 'as' => 'storeEmail']);

// réservation par un envoi de mail
Route::get('resamail', 'ResaMailController@getForm');
Route::post('resamail', 'ResaMailController@postForm');

// réservation par la base mysql
Route::get('resa', 'ResaController@getForm');
Route::post('resa', ['uses' => 'ResaController@postForm', 'as' => 'storeResa']);

// nettoyer l'installation sur le serveur distant
Route::get('/clearcache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});

// authentification 
//route de connexion et identification
Route::get('/connexion', 'ConnexionController@formulaire');
Route::post('/connexion', 'ConnexionController@traitement');

Route::get('board', function() {
// pages qui seront redirigées pour les utilisateurs authentifiés

})->middleware('auth');

// ADMIN- a défaut de pouvoir configurer l'authentification je présente une ébauche des fonctions d'administration sans la sécurité de l'authentification
// index du module administration
Route::get('admin', 'AdminController@index');

// liste des chambres de l'hotel
Route::get('roomsList', function () {
    $roomsList = App\AdminRooms::all();

    return view('roomsList', [
        'roomsList' => $roomsList
    ]);
});

// liste des demandes réservations 
Route::get('adminResa', function () {
    $adminResa = App\Resa::all();

    return view('adminResa', [
        'adminResa' => $adminResa
    ]);
});

//formulaire pour créer les chambres manuellement
Route::get('adminRooms', 'AdminRoomsController@getForm');
Route::post('adminRooms', ['uses' => 'AdminRoomsController@postForm', 'as' => 'storeAdminRoom']);


