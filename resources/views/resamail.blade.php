@extends('template')

@section('contenu')
	<div class="col-sm-offset-3 col-sm-6">
		<div class="panel panel-info">
			<div class="panel-heading"> Envoyez-nous vos besoins </div>
			<div class="panel-body"> 
				{!! Form::open(['url' => 'resamail']) !!}
					<div class="form-group {!! $errors->has('nom') ? 'has-error' : '' !!}">
						{!! Form::text('nom', null, ['class' => 'form-control', 'placeholder' => 'Votre nom']) !!}
						{!! $errors->first('nom', '<small class="help-block">Nom avec des lettres</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('prenom') ? 'has-error' : '' !!}">
						{!! Form::text('prenom', null, ['class' => 'form-control', 'placeholder' => 'Votre prénom']) !!}
						{!! $errors->first('prenom', '<small class="help-block">Prénom avec des lettres</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
						{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Votre email']) !!}
						{!! $errors->first('email', '<small class="help-block">Adresse email</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
						{!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Votre numéro de téléphone']) !!}
						{!! $errors->first('phone', '<small class="help-block">Numéro de téléphone</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('texte') ? 'has-error' : '' !!}">
						{!! Form::number ('personnes', null, ['class' => 'form-control', 'placeholder' => 'Pour combien de personnes ']) !!}
						{!! $errors->first('personnes', '<small class="help-block">:combien de personnes</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('checkbox') ? 'has-error' : '' !!}">
						{!! Form::checkbox ('chambre[]', '1', ['class' => 'form-control', 'placeholder' => 'chambre1']) !!}
						{!! $errors->first('chambre1', '<small class="help-block">:chambre 1 ?</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('checkbox') ? 'has-error' : '' !!}">
						{!! Form::checkbox ('chambre[]', '2', ['class' => 'form-control', 'placeholder' => 'chambre2']) !!}
						{!! $errors->first('chambre2', '<small class="help-block">:chambre 2 ?</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('texte') ? 'has-error' : '' !!}">
						{!! Form::date ('checkin', null, ['class' => 'form-control', 'placeholder' => 'Date arrivée']) !!}
						{!! $errors->first('checkin', '<small class="help-block">Date ultérieure</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('texte') ? 'has-error' : '' !!}">
						{!! Form::date ('checkout', null, ['class' => 'form-control', 'placeholder' => 'Date départ']) !!}
						{!! $errors->first('checkout', '<small class="help-block">Date ultérieure</small>') !!}
					</div>
					{!! Form::submit('Envoyer !', ['class' => 'btn btn-info pull-right']) !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection