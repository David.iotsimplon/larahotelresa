@extends('templateAdmin')

@section('contenu')
    <h1>Les réservations</h1>

    <table class="col-sm-offset-3 col-sm-6" border="2px" align="center">
    	<tr class="panel panel-info">
    		<td  class="panel-heading">
    			Nom
    		</td>
    		<td  class="panel-heading">
    			email
    		</td>
    		<td  class="panel-heading">
    			Téléphone
    		</td>
    		<td  class="panel-heading">
    			Personnes
    		</td>
    		<td  class="panel-heading">
    			Arrivée
    		</td>
    		<td  class="panel-heading">
    			Départ
    		</td>
    		<td  class="panel-heading">
    			Chambre
    		</td>
    	</tr>
    
        @foreach($adminResa as $resa)
            <tr  class="panel panel-info">
            	<td>{{ $resa->nom .' '.$resa->prenom }}</td><td>{{ $resa->email }}</td><td>{{ $resa->phone }}</td><td>{{ $resa->personnes }}</td><td>{{ $resa->checkin }}</td><td>{{ $resa->checkout }}</td><td>{{ $resa->room }}</td>
            </tr>
        @endforeach
    </table>
    
@endsection