@extends('template')

@section('contenu')
    <br>
	<div class="col-sm-offset-3 col-sm-6">
		<div class="panel panel-info">
			<div class="panel-heading">Réservation</div>
			<div class="panel-body"> 
				Merci. Votre demande de réservation a bien été prise en compte. Vous recevrez une réponse rapidement.
			</div>
		</div>
	</div>
@endsection