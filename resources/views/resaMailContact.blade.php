<!DOCTYPE html> <!-- contenu du mail qui sera envoyé avec les éléments de confirmation -->
<html lang="fr">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Réservation</h2>
    <p>Réception d'une demande de réservation avec les éléments suivants :</p>
    <ul>
      <li><strong>Nom</strong> : {{ $nom }}</li>
      <li><strong>Prénom</strong> : {{ $prenom }}</li>
      <li><strong>Email</strong> : {{ $email }}</li>
      <li><strong>Téléphone</strong> : {{ $phone }}</li>
      <li><strong>Personnes</strong> : {{ $personnes }}</li>
      <li><strong>Chambre</strong> : {{ $chambre }}</li>
      <li><strong>Checkin</strong> : {{ $checkin }}</li>
      <li><strong>Checkout</strong> : {{ $checkout }}</li>
    </ul>
  </body>
</html>