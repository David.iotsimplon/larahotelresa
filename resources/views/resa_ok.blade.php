@extends('template')

@section('contenu')
    <br>
	<div class="col-sm-offset-3 col-sm-6">
		<div class="panel panel-info">
			<div class="panel-heading">Demande résevation effectuée</div>
			<div class="panel-body"> 
				Merci. Nous validons votre réservation dès que possible.
			</div>
		</div>
	</div>
@endsection