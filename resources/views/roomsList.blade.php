@extends('templateAdmin')

@section('contenu')
    <h1>Les chambres</h1>

    <table class="col-sm-offset-3 col-sm-6" border="2px" align="center">
    	<tr class="panel panel-info">
    		<td  class="panel-heading">
    			Numéro
    		</td>
    		<td  class="panel-heading">
    			Lits
    		</td>
    		<td  class="panel-heading">
    			Clients par chambre
    		</td>
    		<td  class="panel-heading">
    			Taille
    		</td>
    	</tr>
    
        @foreach($roomsList as $room)
            <tr  class="panel panel-info">
            	<td>{{ $room->number }}</td><td>{{ $room->bed }}</td><td>{{ $room->customer }}</td><td>{{ $room->size }}</td>
            </tr>
        @endforeach
    </table>
    
@endsection