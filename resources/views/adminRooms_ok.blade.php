@extends('templateAdmin')

@section('contenu')
    <br>
	<div class="col-sm-offset-3 col-sm-6">
		<div class="panel panel-info">
			<div class="panel-heading">Chambre enregistrée</div>
			<div class="panel-body"> 
				Nouvelle chambre enregistrée.
			</div>
		</div>
	</div>
@endsection