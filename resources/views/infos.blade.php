@extends('template')

@section('contenu')
    {!! Form::open(['url' => 'users']) !!}
        {!! Form::label('nom', 'Entrez votre nom : ') !!}
        {!! Form::text('nom') !!}
        {!! Form::label('prenom', 'Entrez votre prénom : ') !!}
        {!! Form::text('prenom') !!}
        {!! Form::submit('Envoyer !') !!}
    {!! Form::close() !!}
@endsection