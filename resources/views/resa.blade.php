@extends('template')

@section('contenu')
    <div class="col-sm-offset-4 col-sm-4">
		<div class="panel panel-info">
			<div class="panel-heading">Demande réservation chambre</div>
			<div class="panel-body"> 
				{!! Form::open(['route' => 'storeResa']) !!}
					<div class="form-group {!! $errors->has('nom') ? 'has-error' : '' !!}">
						{!! Form::text('nom', null, ['class' => 'form-control', 'placeholder' => 'Votre nom']) !!}
						{!! $errors->first('nom', '<small class="help-block">Nom avec des lettres</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('nom') ? 'has-error' : '' !!}">
						{!! Form::text('prenom', null, ['class' => 'form-control', 'placeholder' => 'Votre prénom']) !!}
						{!! $errors->first('prenom', '<small class="help-block">Prénom avec des lettres</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
						{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Votre email']) !!}
						{!! $errors->first('email', '<small class="help-block">Adresse email</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
						{!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Votre numéro de téléphone']) !!}
						{!! $errors->first('phone', '<small class="help-block">Numéro de téléphone</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('texte') ? 'has-error' : '' !!}">
						{!! Form::number ('personnes', null, ['class' => 'form-control', 'placeholder' => 'Pour combien de personnes ']) !!}
						{!! $errors->first('personnes', '<small class="help-block">:combien de personnes</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('texte') ? 'has-error' : '' !!}">
						{!! Form::date ('checkin', null, ['class' => 'form-control', 'placeholder' => 'Date arrivée']) !!}
						{!! $errors->first('checkin', '<small class="help-block">Date ultérieure</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('texte') ? 'has-error' : '' !!}">
						{!! Form::date ('checkout', null, ['class' => 'form-control', 'placeholder' => 'Date départ']) !!}
						{!! $errors->first('checkout', '<small class="help-block">Date ultérieure</small>') !!}
					</div>
					{!! Form::submit('Envoyer !', ['class' => 'btn btn-info pull-right']) !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection