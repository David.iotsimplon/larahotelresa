@extends('templateAdmin')

@section('contenu')
    <div class="col-sm-offset-4 col-sm-4">
		<div class="panel panel-info">
			<div class="panel-heading">Création d'une chambre</div>
			<div class="panel-body"> 
				{!! Form::open(['route' => 'storeAdminRoom']) !!}
					<div class="form-group {!! $errors->has('number') ? 'has-error' : '' !!}">
						{!! Form::number('number', null, ['class' => 'form-control', 'placeholder' => 'Numéro de la chambre']) !!}
						{!! $errors->first('number', '<small class="help-block">Ne peut être déjà utilisé et compris entre 1 et 20</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('bed') ? 'has-error' : '' !!}">
						{!! Form::number('bed', null, ['class' => 'form-control', 'placeholder' => 'Nombre de lits']) !!}
						{!! $errors->first('bed', '<small class="help-block">Nombre de lits entre 1 et 4</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('customer') ? 'has-error' : '' !!}">
						{!! Form::number('customer', null, ['class' => 'form-control', 'placeholder' => 'Nombre de clients']) !!}
						{!! $errors->first('customer', '<small class="help-block">Entre 1 et 4</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('size') ? 'has-error' : '' !!}">
						{!! Form::number('size', null, ['class' => 'form-control', 'placeholder' => 'Taille de la chambre']) !!}
						{!! $errors->first('size', '<small class="help-block">Taille entre 11 et 22</small>') !!}
					</div>
					<div class="form-group {!! $errors->has('texte') ? 'has-error' : '' !!}">
						{!! Form::text ('options', null, ['class' => 'form-control', 'placeholder' => 'Options ']) !!}
						{!! $errors->first('options', '<small class="help-block">Quelques options</small>') !!}
					</div>
					{!! Form::submit('Envoyer !', ['class' => 'btn btn-info pull-right']) !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection