<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminRooms extends Model
{
    //
    protected $table = 'rooms';
    
    public $timestamps = false;

}
