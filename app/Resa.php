<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resa extends Model
{
    //
    protected $table = 'resa';
    
    public $timestamps = false;
}
