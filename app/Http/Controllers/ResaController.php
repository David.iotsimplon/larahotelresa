<?php

namespace App\Http\Controllers;

use App\Resa;
use App\Http\Requests\ResaRequest;
//use App\Repositories\ResaRepositoryInterface;

class ResaController extends Controller
{

    public function getForm()
	{
		return view('resa');
	}

	public function postForm(ResaRequest $request)
	{

		$resa = new Resa;
		$resa->nom = $request->input('nom');
		$resa->prenom = $request->input('prenom');
		$resa->email = $request->input('email');
		$resa->phone = $request->input('phone');
		$resa->personnes = $request->input('personnes');
		$resa->checkin = $request->input('checkin');
		$resa->checkout = $request->input('checkout');
		$resa->room = "0";
		$resa->save();
		return view('resa_ok');
	}

}

