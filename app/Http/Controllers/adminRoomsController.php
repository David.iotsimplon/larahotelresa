<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\adminRooms;
use App\Http\Requests\adminRoomsRequest;

class adminRoomsController extends Controller
{
    //
    public function getForm()
	{
		return view('adminRooms');
	}

  	public function postForm(adminRoomsRequest $request)
	{

		$adminRooms = new adminRooms;
		$adminRooms->number = $request->input('number');
		$adminRooms->bed = $request->input('bed');
		$adminRooms->customer = $request->input('customer');
		$adminRooms->size = $request->input('size');
		$adminRooms->options = $request->input('options');
		$adminRooms->save();
		return view('adminRooms_ok');
	}
}
