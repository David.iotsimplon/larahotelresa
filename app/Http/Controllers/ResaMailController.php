<?php

namespace App\Http\Controllers;

use Mail;
use App\Http\Requests\ResaMailRequest;


class ResaMailController extends Controller
{
    //
    public function getForm()
	{
		return view('resamail');
	}


	public function postForm(ResaMailRequest $request)
	{
		Mail::send('ResaMailContact', $request->all(), function($message) 
		{
			$message->to('dpeyronne@hotmail.fr')->subject('Réservation');
		});

		return view('ResaMailConfirm');
	}

}
