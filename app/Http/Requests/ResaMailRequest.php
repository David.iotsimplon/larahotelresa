<?php

namespace App\Http\Requests;

//use Illuminate\Foundation\Http\FormRequest;

class ResaMailRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nom' => 'required|min:5|max:20|alpha',
            'prenom' => 'required|min:5|max:20|alpha',
            'email' => 'required|email',
            'phone' => 'required|min:10|max:20',
            'personnes' => 'required',
//            'chambre' => 'required',
            'checkin' => 'required|after:today',
            'checkout' => 'required|after:checkin'

        ];
    }
}
