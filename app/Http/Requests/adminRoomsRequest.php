<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class adminRoomsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required|unique:rooms,number|integer|min:1|max:20',
            'bed' => 'required|integer|min:1|max:3',
            'customer' => 'required|integer|min:1|max:4',
            'size' => 'required|integer|min:11|max:22',
            'options' => 'required|min:5|max:50|string'
        ];
    }
}
